// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    cache = require('gulp-cache'),
    notify = require('gulp-notify'),
	plumber = require('gulp-plumber'),
	del = require('del'),
	browserSync = require('browser-sync').create(),
  reload = browserSync.reload;

gulp.task('browser-sync', function() {
    browserSync.init({
       server : {
        baseDir : './source'
       }
       // proxy: "blank"
    });
});

/* PATHS */
var paths = {
	styles : 'source/sass/style.scss',
	scripts : 'source/js/**/*.js',
	images : 'source/images/**/*',
  img : 'source/img/**/*',
	html : 'source/*.html'
}

/* CLEAN */
gulp.task('clean-dist', function() {
  return del(['dist'])
});
gulp.task('clean-build', function() {
  return del(['build'])
});

/* WATCH */
gulp.task('watch',  function() {
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.styles, ['styles']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.img, ['img']);
  gulp.watch(paths.html, ['html']);
});



/*  DEFAULT TASK */
// Styles
gulp.task('styles', function() {
  return sass(paths.styles, { style: 'expanded'})
    .on('error', sass.logError)
    .pipe(plumber())
    .pipe(gulp.dest('dist/css'))
    .pipe(autoprefixer('last 2 version'))
});

// Scripts
gulp.task('scripts',  function() {
  return gulp.src(paths.scripts)
   // .pipe(jshint())
    .pipe(gulp.dest('dist/js'))
});

// Images
gulp.task('images', function() {
  return gulp.src(paths.images)
    .pipe(gulp.dest('dist/images'))
    
});

// Img
gulp.task('img', function() {
  return gulp.src(paths.img)
    .pipe(gulp.dest('dist/img'))
});

// html
gulp.task('html', function() {
  return gulp.src(paths.html)
    .pipe(gulp.dest('dist'))
    .pipe(reload)
});

gulp.task('default', ['dist'], function() {
  gulp.start('browser-sync', 'watch')
});

gulp.task('dist', ['clean-dist'], function() {
  gulp.start('styles','scripts','images', 'img', 'html')
});


/*  BUILD TASK  */
// Styles
gulp.task('styles-build', function() {
  return gulp.src('dist/css/**/*.css')
    .pipe(gulp.dest('build/css'))
    .pipe(concat('style.css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('build/css'))
});

// Scripts
gulp.task('scripts-build',  function() {
  return gulp.src('dist/js/**/*.js')
    .pipe(concat('script.js'))
    .pipe(gulp.dest('build/js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('build/js'))
});

// Images
gulp.task('images-build', function() {
  return gulp.src('dist/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('build/images'))
});

// Img
gulp.task('img-build', function() {
  return gulp.src('dist/img/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('build/img'))
});

gulp.task('build', ['dist', 'clean-build'] , function() {
  gulp.start('styles-build',  'scripts-build', 'images-build' ,  'img-build', 'html');
});


