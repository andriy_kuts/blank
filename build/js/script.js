!function(e,a){"use strict";var n={params:{$window:e(a),bottomMenuBgScrollTop:e("#bottom-menu-bg").length?e("#bottom-menu-bg").offset().top:0,$initEventMenu:e("#js-init-event-menu"),$eventMenuTitle:e("#js-event-menu-title"),$eventMenuImg:e("#js-event-menu-img"),$body:e("body"),$mainWrapper:e(".main-wrapper"),$mainHeader:e("#main-header"),firstSlide:!0,smWidth:992,xsWidth:768,slideInfoWrapper:e(".main-carousel-slide-info"),firstInit:!0},_getQueryVariable:function(e){for(var n=a.location.search.substring(1),i=n.split("&"),t=0;t<i.length;t++){var s=i[t].split("=");if(s[0]==e)return s[1]}},_textCollapse:function(a){var n=a,i=n.children(".inner-text-collapse_text").outerHeight(),t=e("<a>",{href:"#","class":!n.hasClass("opened")&&i>n.height()?"btn btn-primary gray":"btn btn-primary gray js-opened",html:"<div class='icon-wrapper'><i class='icon icon_s-arrow-down'></i></div>"+n.data("text-collapse"),click:function(a){a.preventDefault();var i=e(this);i.hasClass("js-opened")?(i.removeClass("js-opened"),n.removeClass("opened").removeAttr("style")):(n.height(i.data("height")).addClass("opened"),i.addClass("js-opened"))},data:{height:i}}),s=e("<div>",{"class":"inner-text-collapse-btn"});s.append(t);var r=n.next(".inner-text-collapse-btn");r.length&&r.remove(),i>n.height()?n.after(s):n.hasClass("opened")&&(n.removeAttr("style").removeClass("opened"),s.removeClass("js-opened"))},_searchMaxHeight:function(a){var n=0;return a.each(function(){var a=e(this).outerHeight(!0);n=Math.max(n,a)}),n},mainSlider:function(){function a(e,a){var n="undefined"==typeof t?e:t;a="undefined"==typeof a?!1:a,i.params.firstSlide?i.params.firstSlide=!1:(n.wrapper.find(".main-info-block").removeClass("opened"),!a&&n.slideTo(e.activeIndex,!0))}function n(a,n){var s="undefined"==typeof t?a:t;n="undefined"==typeof n?!1:n,i.params.firstSlide?i.params.firstSlide=!1:(e(s.slides[a.activeIndex]).children(".main-info-block").addClass("opened"),!n&&s.slideTo(a.activeIndex,!0))}var i=this;i.params.slideInfoWrapper.css("height",i._searchMaxHeight(i.params.slideInfoWrapper.find(".main-info-block")));var t=new Swiper(".main-carousel-slide-info",{spaceBetween:0,direction:i.params.$window.width()>=i.params.smWidth?"vertical":"horizontal",slidesPerView:1,loopedSlides:10,loop:!0,onTransitionStart:function(){},onTransitionEnd:function(e){i.params.$window.width()<i.params.smWidth&&n(e,!0)},onSlideChangeStart:function(e){i.params.$window.width()<i.params.smWidth&&a(e,!0)},onSlideChangeEnd:function(e){i.params.$window.width()<i.params.smWidth&&n(e,!0)},onSliderMove:function(e){i.params.$window.width()<i.params.smWidth&&a(e,!0)}}),s=new Swiper(".main-carousel",{spaceBetween:0,direction:i.params.$window.width()>=i.params.smWidth?"vertical":"horizontal",slidesPerView:1,pagination:".swiper-pagination",paginationClickable:!0,onTransitionStart:function(){},onTransitionEnd:function(e){n(e)},onSlideChangeStart:function(e){a(e)},onSlideChangeEnd:function(e){n(e)},onSliderMove:function(e){a(e)},loopedSlides:10,loop:!0}),r=new Swiper(".main-carousel-thumbs",{spaceBetween:0,centeredSlides:!0,slidesPerView:5,touchRatio:.2,slideToClickedSlide:!0,direction:"vertical",loopedSlides:10,loop:!0,onTransitionStart:function(){},onTransitionEnd:function(e){n(e)},onSlideChangeStart:function(e){a(e)},onSlideChangeEnd:function(e){n(e)},onSliderMove:function(e){a(e)}});i.params.$window.resize(function(){i.params.$window.width()>=i.params.smWidth?(s.params.control=r,r.params.control=s,t.params.allowSwipeToPrev=!1,t.params.allowSwipeToNext=!1):(s.params.control=t,t.params.control=s,t.params.allowSwipeToPrev=!0,t.params.allowSwipeToNext=!0),t.params.direction=i.params.$window.width()>=i.params.smWidth?"vertical":"horizontal",s.params.direction=i.params.$window.width()>=i.params.smWidth?"vertical":"horizontal",i.params.slideInfoWrapper.css("height",i._searchMaxHeight(i.params.slideInfoWrapper.find(".main-info-block")))}).resize()},basicUI:function(){var a=this;e('[data-toggle="tooltip"]').tooltip(),e(".custom-select").selectpicker({mobile:!0,style:"btn-primary for-select"}),e(".custom-select-header").selectpicker({mobile:!0,style:"for-header-select"}),e("#filter-events-calendar").datepicker(),e(".js-star-rating").on("change","input",function(){e(this).parent().children(".js-current-rating").removeClass().addClass("current-rating js-current-rating current-rating-"+this.value)});var n=0;a.params.$window.resize(function(){n!==a.params.$window.width()&&(n=a.params.$window.width(),e("[data-text-collapse]").each(function(n,i){a._textCollapse(e(i))}))}).resize(),"true"===a._getQueryVariable("show_map")&&e("html, body").stop().animate({scrollTop:e("#map-wrapper").offset().top-a.params.$mainHeader.height()-20},600)},mainMenu:function(){var a=this;e(".js-open-main-menu").on("click",function(n){function i(){a.params.$body.removeClass("opened"),r.removeClass("opened"),s.removeClass("opened"),a.params.$mainHeader.removeClass("opened"),r.off("mouseleave")}function t(){a.params.$body.addClass("opened"),r.addClass("opened"),s.addClass("opened"),a.params.$mainHeader.addClass("opened")}n.preventDefault();var s=e(this),r=e(e(this).data("menu"));s.is(".opened")?(i(),a.params.$window.width()<a.params.smWidth&&(a.params.$mainWrapper.off("click.responsive-menu"),a.params.$mainWrapper.off("touchmove.responsive-menu"))):(t(),a.params.$window.width()>=a.params.smWidth?r.off("mouseleave").on("mouseleave",function(){i()}):a.params.$mainWrapper.on("click.responsive-menu, touchmove.responsive-menu",function(e){e.preventDefault(),i(),a.params.$mainWrapper.off("click.responsive-menu"),a.params.$mainWrapper.off("touchmove.responsive-menu")}))})},mainScrollUI:function(){var n=this;n.params.$body.on("click","a[data-scroll]",function(a){a.preventDefault(),e("html, body").animate({scrollTop:e(e(this).data("scroll")).offset().top-e("#main-header").height()})}),n.params.$initEventMenu.length&&n.params.$mainHeader.addClass("initialized");var i=!0;a.onscroll=function(){var a=(n.params.$window.scrollTop()/n.params.bottomMenuBgScrollTop+.8,n.params.$window.scrollTop()/n.params.bottomMenuBgScrollTop),t=0,s=e(".mouse-icon");n.params.$initEventMenu.length&&(t=n.params.$window.width()>=n.params.smWidth?n.params.$initEventMenu.offset().top-n.params.$mainHeader.height():n.params.$initEventMenu.offset().top-n.params.$mainHeader.height()),n.params.$initEventMenu.length&&n.params.$window.scrollTop()>t?(n.params.$mainHeader.addClass("rotate"),Modernizr.csstransforms&&i&&(n.params.$mainHeader.addClass("animating"),n.params.$mainHeader.one(e.support.transition.end,function(){n.params.$mainHeader.removeClass("animating"),i=!1})),n.params.$eventMenuTitle.addClass("full-width"),n.params.$eventMenuImg.addClass("transform-to-top")):(i=!0,n.params.$mainHeader.removeClass("rotate"),n.params.$eventMenuTitle.removeClass("full-width"),n.params.$eventMenuImg.removeClass("transform-to-top"),Modernizr.csstransforms&&(n.params.$mainHeader.off(e.support.transition.end),n.params.$mainHeader.removeClass("animating"))),1>a?s.css("opacity",1-a):s.css("opacity",0)}},changeResponsiveElements:function(){var a=this;e(".main-tabs").each(function(n,i){var t=e(i),s=t.find(".nav-tabs li.active"),r=s.prev().length&&s.prev().is(".visible-xs");a.params.$window.width()<a.params.xsWidth?r&&(t.find(".nav-tabs li.active").removeClass("active"),t.find(".nav-tabs li.visible-xs:first-child").addClass("active"),t.find(".tab-content .tab-pane.active").removeClass("active"),t.find(".tab-content .tab-pane.visible-xs:first-child").addClass("active")):t.find(".nav-tabs li.active.visible-xs").length&&(t.find(".nav-tabs li.active.visible-xs").removeClass("active"),t.find(".nav-tabs li:first-child").next().addClass("active"),t.find(".tab-content .tab-pane.active").removeClass("active"),t.find(".tab-content .tab-pane.visible-xs").next().addClass("active")),t.find(".tab-content .tab-pane *[data-text-collapse]").each(function(n,i){a._textCollapse(e(i))})})},init:function(){var e=this;e.mainSlider(),e.basicUI(),e.mainMenu(),e.mainScrollUI(),e.params.$window.resize(function(){e.changeResponsiveElements()}).resize()}};n.init()}(jQuery,window,document);
// AMD support (Thanks to @FagnerMartinsBrack)
;(function(factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else {
    factory(jQuery);
  }
})(function($){
  'use strict';

  var instances = [],
      matchers  = [],
      defaultOptions  = {
        precision: 100, // 0.1 seconds, used to update the DOM
        elapse: false
      };
  // Miliseconds
  matchers.push(/^[0-9]*$/.source);
  // Month/Day/Year [hours:minutes:seconds]
  matchers.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/
    .source);
  // Year/Day/Month [hours:minutes:seconds] and
  // Year-Day-Month [hours:minutes:seconds]
  matchers.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/
    .source);
  // Cast the matchers to a regular expression object
  matchers = new RegExp(matchers.join('|'));
  // Parse a Date formatted has String to a native object
  function parseDateString(dateString) {
    // Pass through when a native object is sent
    if(dateString instanceof Date) {
      return dateString;
    }
    // Caste string to date object
    if(String(dateString).match(matchers)) {
      // If looks like a milisecond value cast to number before
      // final casting (Thanks to @msigley)
      if(String(dateString).match(/^[0-9]*$/)) {
        dateString = Number(dateString);
      }
      // Replace dashes to slashes
      if(String(dateString).match(/\-/)) {
        dateString = String(dateString).replace(/\-/g, '/');
      }
      return new Date(dateString);
    } else {
      throw new Error('Couldn\'t cast `' + dateString +
        '` to a date object.');
    }
  }
  // Map to convert from a directive to offset object property
  var DIRECTIVE_KEY_MAP = {
    'Y': 'years',
    'm': 'months',
    'n': 'daysToMonth',
    'w': 'weeks',
    'd': 'daysToWeek',
    'D': 'totalDays',
    'H': 'hours',
    'M': 'minutes',
    'S': 'seconds'
  };
  // Returns an escaped regexp from the string
  function escapedRegExp(str) {
    var sanitize = str.toString().replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
    return new RegExp(sanitize);
  }
  // Time string formatter
  function strftime(offsetObject) {
    return function(format) {
      var directives = format.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
      if(directives) {
        for(var i = 0, len = directives.length; i < len; ++i) {
          var directive   = directives[i]
              .match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),
            regexp    = escapedRegExp(directive[0]),
            modifier  = directive[1] || '',
            plural    = directive[3] || '',
            value     = null;
            // Get the key
            directive = directive[2];
          // Swap shot-versions directives
          if(DIRECTIVE_KEY_MAP.hasOwnProperty(directive)) {
            value = DIRECTIVE_KEY_MAP[directive];
            value = Number(offsetObject[value]);
          }
          if(value !== null) {
            // Pluralize
            if(modifier === '!') {
              value = pluralize(plural, value);
            }
            // Add zero-padding
            if(modifier === '') {
              if(value < 10) {
                value = '0' + value.toString();
              }
            }
            // Replace the directive
            format = format.replace(regexp, value.toString());
          }
        }
      }
      format = format.replace(/%%/, '%');
      return format;
    };
  }
  // Pluralize
  function pluralize(format, count) {
    var plural = 's', singular = '';
    if(format) {
      format = format.replace(/(:|;|\s)/gi, '').split(/\,/);
      if(format.length === 1) {
        plural = format[0];
      } else {
        singular = format[0];
        plural = format[1];
      }
    }
    if(Math.abs(count) === 1) {
      return singular;
    } else {
      return plural;
    }
  }
  // The Final Countdown
  var Countdown = function(el, finalDate, options) {
    this.el       = el;
    this.$el      = $(el);
    this.interval = null;
    this.offset   = {};
    this.options  = $.extend({}, defaultOptions);
    // console.log(this.options);
    // Register this instance
    this.instanceNumber = instances.length;
    instances.push(this);
    // Save the reference
    this.$el.data('countdown-instance', this.instanceNumber);
    // Handle options or callback
    if (options) {
      // Register the callbacks when supplied
      if(typeof options === 'function') {
        this.$el.on('update.countdown', options);
        this.$el.on('stoped.countdown', options);
        this.$el.on('finish.countdown', options);
      } else {
        this.options = $.extend({}, defaultOptions, options);
      }
    }
    // Set the final date and start
    this.setFinalDate(finalDate);
    this.start();
  };
  $.extend(Countdown.prototype, {
    start: function() {
      if(this.interval !== null) {
        clearInterval(this.interval);
      }
      var self = this;
      this.update();
      this.interval = setInterval(function() {
        self.update.call(self);
      }, this.options.precision);
    },
    stop: function() {
      clearInterval(this.interval);
      this.interval = null;
      this.dispatchEvent('stoped');
    },
    toggle: function() {
      if (this.interval) {
        this.stop();
      } else {
        this.start();
      }
    },
    pause: function() {
      this.stop();
    },
    resume: function() {
      this.start();
    },
    remove: function() {
      this.stop.call(this);
      instances[this.instanceNumber] = null;
      // Reset the countdown instance under data attr (Thanks to @assiotis)
      delete this.$el.data().countdownInstance;
    },
    setFinalDate: function(value) {
      this.finalDate = parseDateString(value); // Cast the given date
    },
    update: function() {
      // Stop if dom is not in the html (Thanks to @dleavitt)
      if(this.$el.closest('html').length === 0) {
        this.remove();
        return;
      }
      var hasEventsAttached = $._data(this.el, 'events') !== undefined,
          now               = new Date(),
          newTotalSecsLeft;
      // Create an offset date object
      newTotalSecsLeft = this.finalDate.getTime() - now.getTime(); // Millisecs
      // Calculate the remaining time
      newTotalSecsLeft = Math.ceil(newTotalSecsLeft / 1000); // Secs
      // If is not have to elapse set the finish
      newTotalSecsLeft = !this.options.elapse && newTotalSecsLeft < 0 ? 0 :
        Math.abs(newTotalSecsLeft);
      // Do not proceed to calculation if the seconds have not changed or
      // does not any event attached
      if (this.totalSecsLeft === newTotalSecsLeft || !hasEventsAttached) {
        return;
      } else {
        this.totalSecsLeft = newTotalSecsLeft;
      }
      // Check if the countdown has elapsed
      this.elapsed = (now >= this.finalDate);
      // Calculate the offsets
      this.offset = {
        seconds     : this.totalSecsLeft % 60,
        minutes     : Math.floor(this.totalSecsLeft / 60) % 60,
        hours       : Math.floor(this.totalSecsLeft / 60 / 60) % 24,
        days        : Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
        daysToWeek  : Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
        daysToMonth : Math.floor(this.totalSecsLeft / 60 / 60 / 24 % 30.4368),
        totalDays   : Math.floor(this.totalSecsLeft / 60 / 60 / 24),
        weeks       : Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
        months      : Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30.4368),
        years       : Math.abs(this.finalDate.getFullYear() - now.getFullYear())
      };
      // Dispatch an event
      if(!this.options.elapse && this.totalSecsLeft === 0) {
        this.stop();
        this.dispatchEvent('finish');
      } else {
        this.dispatchEvent('update');
      }
    },
    dispatchEvent: function(eventName) {
      var event = $.Event(eventName + '.countdown');
      event.finalDate     = this.finalDate;
      event.elapsed       = this.elapsed;
      event.offset        = $.extend({}, this.offset);
      event.strftime      = strftime(this.offset);
      this.$el.trigger(event);
    }
  });
  // Register the jQuery selector actions
  $.fn.countdown = function() {
    var argumentsArray = Array.prototype.slice.call(arguments, 0);
    return this.each(function() {
      // If no data was set, jQuery.data returns undefined
      var instanceNumber = $(this).data('countdown-instance');
      // Verify if we already have a countdown for this node ...
      // Fix issue #22 (Thanks to @romanbsd)
      if (instanceNumber !== undefined) {
        var instance = instances[instanceNumber],
          method = argumentsArray[0];
        // If method exists in the prototype execute
        if(Countdown.prototype.hasOwnProperty(method)) {
          instance[method].apply(instance, argumentsArray.slice(1));
        // If method look like a date try to set a new final date
        } else if(String(method).match(/^[$A-Z_][0-9A-Z_$]*$/i) === null) {
          instance.setFinalDate.call(instance, method);
          // Allow plugin to restart after finished
          // Fix issue #38 (thanks to @yaoazhen)
          instance.start();
        } else {
          $.error('Method %s does not exist on jQuery.countdown'
            .replace(/\%s/gi, method));
        }
      } else {
        // ... if not we create an instance
        new Countdown(this, argumentsArray[0], argumentsArray[1]);
      }
    });
  };
});
